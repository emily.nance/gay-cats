export const dino = new URL("../assets/dino.png?as=webp&width=256", import.meta.url).toString();

export const levels = {
    "title": new URL("../assets/levels/title.png?as=webp&width=768", import.meta.url).toString(),
    "alley": new URL("../assets/levels/alley.png?as=webp&width=768", import.meta.url).toString(),
    "house_front": new URL("../assets/levels/house_front.png?as=webp&width=768", import.meta.url).toString(),
    "moonbucks": new URL("../assets/levels/moonbucks.png?as=webp&width=768", import.meta.url).toString(),
    "living_room": new URL("../assets/levels/living_room.png?as=webp&width=768", import.meta.url).toString(),
};

export const characters = {
    "chubchub_neutral": new URL("../assets/characters/chubchub_neutral.png?as=webp", import.meta.url).toString(),
    "chubchub_openmouth": new URL("../assets/characters/chubchub_openmouth.png?as=webp", import.meta.url).toString(),
    "chubchub_smile": new URL("../assets/characters/chubchub_smile.png?as=webp", import.meta.url).toString(),
};