import * as pixi from "pixi.js"

export class GameState {
    character_name: string;
    app: pixi.Application;
    background: pixi.Sprite;

    main_actor: pixi.Sprite;

    constructor(app: pixi.Application){
        this.app = app;

        this.background = new pixi.Sprite();
        this.background.x = 0;
        this.background.y = 0;
        this.background.width = 768;
        this.background.height = 432;
        this.background.zIndex = -100;
        this.app.stage.addChild(this.background);

        this.main_actor = new pixi.Sprite();
        this.main_actor.anchor.x = 1
        this.main_actor.anchor.y = 1
        this.main_actor.x = 768
        this.main_actor.y = 432
        this.main_actor.zIndex = 1;
        this.main_actor.scale.x = 0.5;
        this.main_actor.scale.y = 0.5;

        this.app.stage.addChild(this.main_actor);
    }
}