import character_data from "../assets/characters/characters.yml";
import * as assets from './assets';
import * as pixi from 'pixi.js'
import { GameState } from "./gamestate";

class CharacterEmotion {
    texture: pixi.Texture;

    constructor(resource_name: string){
        this.texture = pixi.Texture.from(assets.characters[resource_name]);
    }
}

class Character {
    emotions: { [index: string]: CharacterEmotion } = {};

    constructor(source: CharacterData) {
        for(const emotion in source.emotions){
            this.emotions[emotion] = new CharacterEmotion(source.emotions[emotion]);
        }
    }
}

export default class Characters {
    static characters: { [index: string]: Character } = {};

    static load(){
        for(const character_id in character_data){
            Characters.characters[character_id] = new Character(character_data[character_id]);
        }
    }

    static set_main_actor(char: PartCharacter, state: GameState){
        const character_emotion = Characters.characters[char.id].emotions[char.emotion];
        state.main_actor.texture = character_emotion.texture;
    }
}

