import dialogue_tree from "../assets/dialogue.yml";
import * as MarkdownIt from "markdown-it";
import { GameState } from "./gamestate";
import * as handlebars from "handlebars";
import Levels from "./levels";
import Characters from "./characters";

const maintext = document.getElementById("maintext");
const options_list = document.getElementById("interactions");



class TextProcessor {
    md: MarkdownIt;
    hbs: typeof handlebars;

    processText(template: string, state: GameState): string {
        const compiled = this.hbs.compile(template);
        const context = {
            state,
            name: state.character_name,
            "mc": {name: state.character_name, color: "red"}
        };
        Object.assign(context, dialogue_tree.characters);
        return compiled(context);
    }

    render(template: string, state: GameState): string {
        return this.md.render(this.processText(template, state));
    }

    renderInline(template: string, state: GameState): string {
        return this.md.renderInline(this.processText(template, state));
    }

    constructor() {
        this.md = new MarkdownIt({typographer: true, html: true});
        this.hbs = handlebars.create();

        const old_this = this;
        function quote_helper(character, options){
            return new handlebars.SafeString(
                `<span class=quote style="color:${character.color}">
                    ${character.name}: "${old_this.renderInline(options.fn(this), this.state)}"
                </span>`
            );
        }

        this.hbs.registerHelper("quote", quote_helper);
        this.hbs.registerHelper("q", quote_helper);
    }
}

const processor = new TextProcessor();

export function goto_part(part_id: string, state: GameState){
    const part = dialogue_tree.parts[part_id];

    const rendered_maintext = processor.render(part.text, state);
    maintext.innerHTML = rendered_maintext;

    if(part.scene !== undefined){
        Levels.set_scene(part.scene, state);
    }

    if(part.actors !== undefined){
        if(part.actors.length === 1){
            Characters.set_main_actor(part.actors[0], state);
        }
    }

    options_list.innerHTML = ""
    if(part.next !== undefined){
        const option_element = document.createElement("li");
        option_element.classList.add("interaction");
        option_element.innerHTML = processor.renderInline("...", state);
        option_element.addEventListener("click", () => {
            goto_part(part.next, state);
        })
        options_list.appendChild(option_element);
    } else if(part.custom_options !== undefined){
        custom_interactions[part.custom_options](state);
    } else if(part.options !== undefined){
        for(const option of part.options){
            const option_element = document.createElement("li");
            option_element.classList.add("interaction");
            option_element.innerHTML = processor.renderInline(option.text, state);
            option_element.addEventListener("click", () => {
                goto_part(option.to, state);
            })
            options_list.appendChild(option_element);
        }
    }
}

const custom_interactions  = {
    name_selection(state: GameState) {
        const names = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        for(const name of names){
            const option_element = document.createElement("li");
            option_element.classList.add("interaction");
            option_element.innerText = name;
            option_element.addEventListener("click", () => {
                state.character_name = name;
                goto_part("welcome name", state);
            });
            options_list.appendChild(option_element);
        }
    }
};