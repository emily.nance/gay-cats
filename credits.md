# Credits

Julia Scheaffer (she/her) - Programmer
Julia Jenks (she/they) - World artist
Emmitt Odney (he/him) - Feline Character artist
Rebecca Smith (she/they) - Writing
Emily Nance (she/her) - Character artist
Ella Phillips (she/they) - Human Character artist
Beau (he/him) - Writing
